# Team #1

- Our final project for Spring Boot Class (CEJV 559).


### Installing

 - Import the Project as Spring Boot Project.

 - Make sure the MySQL info matches the WorkBench Information you have on your local machine.

 - You will see it running afterwards.


## Built With

* [Spring Boot](https://spring.io/projects/spring-boot) - Spring Boot is a Java Framework
* [Maven](https://maven.apache.org/) - Code Deployment Tool
* [Hibernate](https://hibernate.org/) - Used to generate RSS Feeds
* [JPA](https://spring.io/projects/spring-data-jpa) - Spring Data JPA
* [Java Language](https://en.wikipedia.org/wiki/Java_(programming_language)) - Java Programming Language
* [POM.XML](https://maven.apache.org/guides/introduction/introduction-to-the-pom.html#:~:text=Available%20Variables-,What%20is%20a%20POM%3F,default%20values%20for%20most%20projects.) - Contains all Dependencies
* [MVC Model (Model View Controller)](https://en.wikipedia.org/wiki/Model%E2%80%93view%E2%80%93controller#:~:text=Model%E2%80%93view%E2%80%93controller%20(usually,logic%20into%20three%20interconnected%20elements.) - Model View Controller Design Pattern
* [MySQL Connectivity (with MySQL WorkBench)](https://www.mysql.com/) - MySQL Database 
* [Thymeleaf Layout Framework](https://www.thymeleaf.org/) - Thymeleaf Web Layout used in Spring 
* [Spring Security Authentification System](https://spring.io/projects/spring-security) - Used for Login / Registration Systems
* [Bootstrap Web Framework](https://getbootstrap.com/) - Used to design web pages
* [JUnits](https://junit.org/junit5/) - Used to test methods and code in general


## Authors

* Alexis Chartrand - Front end / Back End Work 
* Osman - Back End Work with some Front End
* Bill - Mostly Back End Work 
* Qingwei - Designed the Database Model  


