package com.concordia.crud.controller;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import com.concordia.email.impl.EmailServiceImpl;
import com.concordia.exception.EmailException;

@Controller
public class HomeController {

    @GetMapping("/index")
    public String root() {
        return "index";
    }

//    @GetMapping("/user")
//    public String userIndex() {
//        return "user/index";
//    }
//    
//    @GetMapping("/register")
//    public String register() {
//        return "register";
//    }
//
//    @GetMapping("/login")
//    public String login() {
//        return "login";
//    }
//    
    @GetMapping("/about")
    public String about() {
        return "about";
    }
    
    @GetMapping("/thankyou")
    public String thankyou() throws EmailException {
        ApplicationContext context = new ClassPathXmlApplicationContext("Spring-Mail.xml");  
		EmailServiceImpl email = (EmailServiceImpl) context.getBean("emailServiceImpl");
		String senderEmailId = "chartrandalex20@gmail.com";
		String receiverEmailId = "chartrandalex20@gmail.com";
		String subject = "Ecommerce Website Alert";
		String message = "Thank you for shopping with us today?";
	    email.sendEmail(senderEmailId, receiverEmailId, subject, message);
	    return "thankyou";
    }
    
    @GetMapping("/thankyouemail")
    public String thankyouemail() throws EmailException {
        ApplicationContext context = new ClassPathXmlApplicationContext("Spring-Mail.xml");  
		EmailServiceImpl email = (EmailServiceImpl) context.getBean("emailServiceImpl");
		String senderEmailId = "chartrandalex20@gmail.com";
		String receiverEmailId = "chartrandalex20@gmail.com";
		String subject = "Ecommerce Website Alert";
		String message = "Thank you for shopping with us today?";
	    email.sendEmail(senderEmailId, receiverEmailId, subject, message);
	    return "thankyouemail";
    }
//    
//    @GetMapping("/product")
//    public String products() {
//        return "product";
//    }
//    
    @GetMapping("/cart")
    public String cart() {
        return "cart";
    }
    
    @GetMapping("/faq")
    public String faq() {
        return "faq";
    }
    
    @GetMapping("/contact")
    public String contact() {
        return "contact";
    }

    @GetMapping("/access-denied")
    public String accessDenied() {
        return "/error/access-denied";
    }

}
