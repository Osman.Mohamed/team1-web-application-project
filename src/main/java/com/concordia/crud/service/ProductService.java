package com.concordia.crud.service;

import java.util.List;

import org.springframework.data.domain.Page;

import com.concordia.crud.dto.ProductDTO;
import com.concordia.crud.model.Product;
import com.concordia.exception.ServiceException;

public interface ProductService {

	void deleteProduct(long id) throws ServiceException;

	List<Product> getAllProducts();

	Product updateProduct(Product product, long id) throws ServiceException;

	ProductDTO getProduct(long id) throws ServiceException;

	// Pagination and sorting
	Page<ProductDTO> findPaginated(int pageNumber, int pageSize) throws ServiceException;

	Product addProduct(Product product) throws ServiceException;

}
