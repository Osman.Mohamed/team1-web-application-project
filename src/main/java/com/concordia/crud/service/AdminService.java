package com.concordia.crud.service;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;

import com.concordia.crud.model.Admin;
import com.concordia.crud.model.Product;
import com.concordia.exception.ServiceException;

public interface AdminService {

	List<Admin> getAllProducts() throws ServiceException;

	Admin addProduct(Admin admin) throws ServiceException;

	Admin updateProduct(Admin admin, long id) throws ServiceException;

	void deleteProduct(long id) throws ServiceException;

	Optional<Admin> getProduct(long id) throws ServiceException;

	Page<Product> getPaginated(int pageNumber, int pageSize) throws ServiceException;

}
