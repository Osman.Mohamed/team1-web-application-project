package com.concordia.crud.service;

import com.concordia.crud.dto.OrderDTO;
import com.concordia.exception.ServiceException;

/**
 * Order Service Interface
 *
 * @author Alexis
 *
 */

public interface OrderService {

	OrderDTO getOrder(Long id) throws ServiceException;

	void saveOrder(OrderDTO orderDTO) throws ServiceException;

	void deleteOrder(Long id) throws ServiceException;

	void updateOrder(OrderDTO orderDTO) throws ServiceException;

}
