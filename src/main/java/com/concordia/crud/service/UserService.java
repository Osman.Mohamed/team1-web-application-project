package com.concordia.crud.service;

import com.concordia.crud.dto.UserDTO;
import com.concordia.exception.ServiceException;

public interface UserService {

	void save(UserDTO userDTO) throws ServiceException;

}
