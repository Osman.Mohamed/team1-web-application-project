package com.concordia.crud.service;

import com.concordia.crud.dto.CartDTO;
import com.concordia.exception.ServiceException;

/**
 * Cart Service Interface
 *
 * @author Alexis
 *
 */

public interface CartService {

	CartDTO getCart(Long id) throws ServiceException;

	void saveCart(CartDTO cartDTO) throws ServiceException;

	void deleteCart(Long id) throws ServiceException;

	void updateCart(CartDTO cartDTO) throws ServiceException;

}
