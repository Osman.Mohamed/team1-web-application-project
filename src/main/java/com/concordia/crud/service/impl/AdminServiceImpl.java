package com.concordia.crud.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.concordia.crud.model.Admin;
import com.concordia.crud.model.Product;
import com.concordia.crud.repository.AdminRepository;
import com.concordia.crud.repository.ProductRepository;
import com.concordia.crud.service.AdminService;
import com.concordia.exception.ServiceException;

@Service
public class AdminServiceImpl implements AdminService {

	@Autowired
	private AdminRepository adminRepository;

	@Autowired
	ProductRepository productRepository;

	@Override
	public List<Admin> getAllProducts() {

		List<Admin> products = new ArrayList<>();
		adminRepository.findAll().forEach(products::add);

		return products;
	}

	@Override
	@Transactional
	public Admin addProduct(Admin admin)throws ServiceException {

		return adminRepository.save(admin);
	}

	@Override
	@Transactional
	public Admin updateProduct(Admin admin, long id)throws ServiceException {

		return adminRepository.save(admin);

	}

	@Override
	@Transactional
	public void deleteProduct(long id)throws ServiceException {
		adminRepository.deleteById(id);

	}

	@Override
	public Optional<Admin> getProduct(long id)throws ServiceException {

		return adminRepository.findById(id);

//		return productRepository.findById(id);

	}

	@Override
	public Page<Product> getPaginated(int pageNumber, int pageSize)throws ServiceException {
		Pageable pageable = PageRequest.of(pageNumber - 1, pageSize);

		return productRepository.findAll(pageable);
	}

}
