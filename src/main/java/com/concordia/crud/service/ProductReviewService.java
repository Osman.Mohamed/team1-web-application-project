package com.concordia.crud.service;

import java.util.List;

import org.springframework.data.domain.Page;

import com.concordia.crud.dto.ProductReviewDTO;
import com.concordia.exception.ServiceException;

/**
 * Product Review Service Interface
 *
 * @author Alexis
 *
 */

public interface ProductReviewService {

	ProductReviewDTO getProductReview(Long id) throws ServiceException;

	void saveProductReview(ProductReviewDTO productReviewDTO) throws ServiceException;

	void deleteProductReview(Long id) throws ServiceException;

	void updateProductReview(ProductReviewDTO productReviewDTO) throws ServiceException;

	List<ProductReviewDTO> getAllProductReviews() throws ServiceException;

	Page<ProductReviewDTO> findPaginated(int pageNo, int pageSize, String sortField, String sortDirection)
			throws ServiceException;

}
