package com.concordia.crud.service;

import java.util.List;

import org.springframework.data.domain.Page;

import com.concordia.crud.dto.CartItemDTO;
import com.concordia.exception.ServiceException;

/**
 * Cart Item Service Interface
 *
 * @author Alexis
 *
 */

public interface CartItemService {

	CartItemDTO getCartItem(Long id) throws ServiceException;

	void saveCartItem(CartItemDTO cartItemDTO) throws ServiceException;

	void deleteCartItem(Long id) throws ServiceException;

	void updateCartItem(CartItemDTO cartItemDTO) throws ServiceException;

	List<CartItemDTO> getAllCartItems() throws ServiceException;

	Page<CartItemDTO> findPaginated(int pageNo, int pageSize, String sortField, String sortDirection)
			throws ServiceException;

}
