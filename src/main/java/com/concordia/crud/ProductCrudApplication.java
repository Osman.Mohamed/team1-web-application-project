package com.concordia.crud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

import com.concordia.exception.EmailException;



@SpringBootApplication

public class ProductCrudApplication extends SpringBootServletInitializer {

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(ProductCrudApplication.class);
	}

	public static void main(String[] args) throws EmailException {
    	/**
    	 * Sending the Email with the different parameters
    	 *
    	 * @param senderEmailId The Senders Email Address
    	 * @param receiverEmailId The Receivers Email Address
    	 * @param subject The subject of the email.
    	 * @param message The message content in the body which regular text.
    	 * @throws EmailException
    	 */
        SpringApplication.run(ProductCrudApplication.class, args);
	}

}
